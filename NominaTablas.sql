create database EmpresaConstruccionN

use EmpresaConstruccionN

Create table Deptos(
Id_Dpto int identity (1,1) primary key not null,
NombreDpto nvarchar (25) not null
) 

Create table Municipios (
Id_Mun int identity (1,1) primary key not null,
NombreMun nvarchar(25) not null,
Id_dpto int foreign key references Deptos (Id_Dpto) not null
)

Create table Comarcas (
Id_Com int identity (1,1) primary key not null,
NombreComarcas nvarchar (25) not null,
Id_Mun int foreign key references Municipios (Id_Mun) not null
) 

Create Table Cargo (
Id_Cargo int identity (1,1) primary key not null,
N_Cargo varchar (25) not null,
Descripcion varchar (75) not null,
Salario money not null
)

create Table Turno (
Id_Turno int identity (1,1) primary key not null,
Descripcion varchar (75) not null
)

create Table Horario(
Id_Horario int identity (1,1) primary key not null,
Id_Turno int foreign key references Turno(Id_Turno) not null,
Hora_Inicio time,
Hora_Fin time
)

create table Empleados(
Id_Empleado int identity (1,1) primary key not null,
P_Nombre nvarchar (25) not null,
S_Nombre nvarchar (25) not null,
P_Apellido nvarchar (25) not null,
S_Apellido nvarchar (25) not null,
Telefono char(8) check(Telefono like '[5|7|8][0-9][0-9][0-9][0-9][0-9][0-9][0-9]')not null,
Correo nvarchar (75) not null,
Id_Com int foreign key references Comarcas (Id_Com) not null,
Dir text not null,
Id_Cargo int foreign key references Cargo(Id_Cargo) not null 
)

Create table Control_Asis (
Id_Control_Asis int identity (1,1) primary key not null,
Id_Horario int foreign key references Horario(Id_Horario) not null,
Id_Empleado int foreign key references Empleados(Id_Empleado)not null,
Fecha date not null
)

create table Nomina(
Id_Nomina int identity(1,1) primary key not null,
Id_Empleado int foreign key references Empleados(Id_Empleado) not null,
Fecha_Elaboracion date not null,
Id_Cargo int foreign key references Cargo(Id_Cargo) not null,
Salario_base money not null,
Cantidad_HrsExtra money not null,
IR money not null,
InssL money not null,
InssP money not null,
MontoTotal money not null
)

create proc mostrar_nomina
as
select 
e.P_Apellido,
c.N_Cargo,
dbo.inssL(c.[Salario]),
dbo.inssP(c.[Salario])
from 
Empleados e inner join 
Cargo c on e.[Id_Cargo] = c.Id_Cargo

create table Contratos(
Id_Contrato int identity(1,1) primary key not null,
Fecha_Contrato date not null,
VigenciaAños int not null,
Id_Empleado int foreign key references Empleados(Id_Empleado)
)

create table Colilla(
Id_Colilla int identity(1,1) primary key not null,
Id_Empleado int foreign key references Empleados(Id_Empleado) not null,
Id_Cargo int foreign key references Cargo(Id_Cargo) not null,
Id_Contratos int foreign key references Contratos(Id_Contrato) not null,
FechaEmision date not null 
)
