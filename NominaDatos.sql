use EmpresaConstruccionN

-- Departamentos
insert into Deptos values('MANAGUA')
insert into Deptos values('LEON')
insert into Deptos values('CHINANDEGA')
insert into Deptos values('MASAYA')

-- Municipios 
create procedure InsertarMun
@Nom nvarchar(25),
@ID_Dep int
as
declare @Dep as int
set @Dep = (select Id_Dpto from Deptos where Id_Dpto = @ID_Dep)
if(@ID_Dep = @Dep)
begin
	insert into Municipios values(@Nom,@ID_Dep)
end
else
begin
	print 'Error al insertar'
end

InsertarMun 'TICUANTEPE',1
InsertarMun 'MANAGUA',1
InsertarMun 'TELICA',2
InsertarMun 'LEON',2
InsertarMun 'CORINTO',3
InsertarMun 'CATARINA',4

-- Comarcas 
create procedure InsertarCom
@Nom nvarchar(25),
@ID_Mun int
as
declare @Mun as int
set @Mun = (select Id_Mun from Municipios where Id_Mun = @ID_Mun)
if(@ID_Mun = @Mun)
begin
	insert into Comarcas values(@Nom,@ID_Mun)
end
else
begin
	print 'Error al insertar'
end

InsertarCom 'ComarcaA',1
InsertarCom 'ComarcaB',1
InsertarCom 'ComarcaC',2
InsertarCom 'ComarcaD',2
InsertarCom 'ComarcaE',3
InsertarCom 'ComarcaF',4

-- Cargos
insert into Cargo values('Alba�il','Hacer Mezclas',8000)
insert into Cargo values('Supervisor','Controla subordinados',15000)
insert into Cargo values('EncargadoAlmacen','Guarda Inventario',5000)

-- Turnos
insert into Turno values('Matutino')
insert into Turno values('Vespertino')

-- Horarios
update Horario set Hora_Inicio = '08:00:00', Hora_Fin = '14:00:00' where Id_Horario = 1 
update Horario set Hora_Inicio = '13:00:00', Hora_Fin = '19:00:00' where Id_Horario = 2 

-- Empleados
insert into Empleados values ('Juan','Pedro','Castillo','Cuadra','56781236','juanP@yahoo.es',1,'Barrio San Sebastian',1)
insert into Empleados values ('Alberth','Manuel','Ruiz','Matamoros','76889206','AlbeRuiz@outlook.es',2,'Residencial Fatima',2)
insert into Empleados values ('Sergio','Antonio','Estrella','Delgadillo','88560932','SergDel@gmail.com',4,'Barrio Esperanza',3)

select * from Empleados

-- Control Asistencias 
insert into Control_Asis values (1,'05-10-2017',1)
insert into Control_Asis values (2,'08-11-2017',2)
insert into Control_Asis values (2,'08-11-2017',3)

--INSS Laboral
create function INSSL
(@SalarioB money)
returns money
as
begin
	declare @INSSL as money
	set @INSSL= @SalarioB * 0.0625
	
return @INSSL
end

--IR
create function Ded_IR
(@SalarioB money)
returns money
as 
begin
	declare @IR as money
	declare @Deduccion as money
	set @Deduccion = ((@SalarioB - dbo.INSSL(@SalarioB))*12)
	if(@Deduccion > 0 and @Deduccion < 100000.01)
	begin
		set @IR = 0.00
	end
	else
	begin
		if(@Deduccion > 100000 and @Deduccion < 200000.01)
		begin
			set @IR = ((@Deduccion - 100000)*0.15)/12
		end
		else
		begin
			if(@Deduccion > 200000 and @Deduccion < 350000.01)
			begin
				set @IR = (((@Deduccion - 200000)*0.2) + 15000)/12
			end
			else
			begin
				if(@Deduccion > 350000 and @Deduccion < 500000.01)
				begin
					set @IR = (((@Deduccion - 350000)*0.25) + 45000)/12
				end
				else
				begin
					if(@Deduccion > 500000)
					begin
						set @IR = (((@Deduccion - 500000)*0.3) + 82500)/12
					end
				end
			end
		end
	end
	return @IR
end

--INSS Patronal
create function INSSP
(@SalarioN money)
returns money
as
begin
	declare @INSS as money
	set @INSS = @SalarioN * 0.19
return @INSS
end

-- Horas Extras
create function HorasE
(@HorasE float, @SalarioB money)
returns money
as
begin
	declare @Horas as money
	set @Horas = (((@SalarioB/30)/8)*2)*@HorasE
return @Horas
end

-- Nomina
create procedure NominaV
@IDE int,
@Fecha date,
@Cargo int,
@Horas float
as
declare @SalarioB as money
set @SalarioB = (select Salario from Cargo where Id_Cargo = @Cargo)
declare @IDemp as int 
set @IDemp = (select Id_Empleado from Empleados where Id_Empleado = @IDE)
if(@IDE = @IDemp and @IDE = @Cargo)
begin
	declare @TotalIngresos as money
	set @TotalIngresos = @SalarioB + dbo.HorasE(@Horas,@SalarioB)
	declare @SalarioNeto as money
	set @SalarioNeto = @TotalIngresos - dbo.INSSL(@TotalIngresos) - dbo.Ded_IR(@TotalIngresos)

	insert into Nomina values (@IDE,@Fecha,@Cargo,@SalarioB,dbo.HorasE(@Horas,@SalarioB),dbo.Ded_IR(@TotalIngresos),dbo.INSSL(@TotalIngresos),dbo.INSSP(@SalarioNeto),@SalarioNeto)
end
else
begin
	print 'Empleado no registrado o El empleado no pertenece al cargo digitado'
end

NominaV 1,'08-10-2017',1,0
NominaV 2,'08-10-2017',2,10
NominaV 3,'08-10-2017',3,5

-- Contratos
insert into Contratos values ('27/08/2015',2,1) 
insert into Contratos values ('25/05/2016',3,2) 
insert into Contratos values ('05/01/2017',1,3)

