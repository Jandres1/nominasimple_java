use EmpresaConstruccionN

select * from Nom

create view Muni
as
select Id_Mun,NombreMun,D.NombreDpto from Municipios M inner join Deptos D on M.Id_dpto = D.Id_Dpto

create view Com
as
select Id_Com,NombreComarcas,M.NombreMun from Comarcas C inner join Municipios M on M.Id_Mun = C.Id_Mun

create view Hor
as
select Id_Horario,T.Descripcion,Hora_Inicio,Hora_Fin from Horario H inner join Turno T on H.Id_Turno = T.Id_Turno

create view Emp
as
select Id_Empleado,P_Nombre,S_Nombre,P_Apellido,S_Apellido,Telefono,Correo,C.NombreComarcas,Dir,CC.N_Cargo from Empleados E inner join Comarcas C on C.Id_Com = E.Id_Com inner join Cargo CC on CC.Id_Cargo = E.Id_Cargo

create view Asis
as
select CA.Id_Control_Asis,E.P_Nombre,E.P_Apellido,H.Hora_Inicio,H.Hora_Fin,Fecha from Control_Asis CA inner join Horario H on CA.Id_Horario = H.Id_Horario left join Empleados E on E.Id_Empleado = CA.Id_Empleado

create view Nom 
as 
select Id_Nomina,E.P_Nombre,E.P_Apellido,E.S_Nombre,E.S_Apellido,Fecha_Elaboracion,C.N_Cargo,Salario_base,Cantidad_HrsExtra,IR,InssL,InssP,MontoTotal  from Nomina N inner join Empleados E on E.Id_Empleado = N.Id_Empleado inner join Cargo C on C.Id_Cargo = N.Id_Cargo

create view Cont
as 
select c.Id_Contrato,E.P_Nombre,E.P_Apellido,E.S_Nombre,E.S_Apellido,Fecha_Contrato,VigenciaAños from Contratos C inner join Empleados E on E.Id_Empleado = C.Id_Empleado

create procedure VistaN
as
select Id_Nomina,E.P_Nombre,E.P_Apellido,E.S_Nombre,E.S_Apellido,Fecha_Elaboracion,C.N_Cargo,Salario_base,Cantidad_HrsExtra,IR,InssL,InssP,MontoTotal  from Nomina N inner join Empleados E on E.Id_Empleado = N.Id_Empleado inner join Cargo C on C.Id_Cargo = N.Id_Cargo

